(function(window, $){
    var preloader_instances = 0; // set unique ID when multiple instances are required

    var preloader = function(elem, options){
        this.elem = elem;
        this.$elem = $(elem);
        this.$elem.data('plugin.preloader', this);
        this.options = options;
        this.metadata = this.$elem.data('xlabs-preloader-options');
        this.init();
    };

    preloader.prototype = {
        defaults: {
            background: 'none',
            background_opacity: '1',
            color: '#ffffff',
            text: '',
            spinner_font_size: '1em',
            onSet: function(){},
            onDestroy: function(){}
        },
        injectStyles: function(rule) {
            $('<style>' + rule.join("\r\n") + '</style>').appendTo('head');
        },
        init: function(){
            this.config = $.extend({}, this.defaults, this.options, this.metadata);
            this.$elem.data('plugin.preloader', this);
            let plugin = this;

            if(!plugin.$elem.length)
            {
                return;
            }

            // Force relative position on element
            //plugin.$elem.css('position', 'relative');

            // inject styles only once
            //if(typeof window.preloader === "undefined")
            //{
                //console.log('no exist');
                plugin.injectStyles([
                    '._xlabs_preloader {position: absolute; top: 0; left: 0; float: left; width: 100%; height: 100%; display: none; z-index: 9999999;}',
                    '._xlabs_preloader > span {display: inline-block; position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); font-size: 3em; z-index: 10;}',
                    '._xlabs_preloader > span i {}',
                    '._xlabs_preloader > span span.text {font-size: .3em;}'
                ]);
            //}

            let container = $('<div/>').css({
                'opacity': plugin.config.background_opacity,
                'background': plugin.config.background,
            }).addClass('_xlabs_preloader');
            let text = $('<span/>').css({
                'color': plugin.config.color
            });
            let spinner = $('<i class="fas fa-spinner">').css({
                'color': plugin.config.color,
                'font-size': plugin.config.spinner_font_size
            }).addClass('_rotate');

            plugin.$elem.append(
                container.append(
                    text.append(
                        spinner
                    )
                )
            );
            if(plugin.config.text && plugin.config.text != '')
            {
                text.append($('<br /><span class="text">' + plugin.config.text + '</span>'));
            }
        },
        show: function() {
            let plugin = this;
            plugin.$elem.find('> ._xlabs_preloader').show();
            plugin.config.onSet();
        },
        hide: function() {
            let plugin = this;
            plugin.$elem.find('> ._xlabs_preloader').hide();
        },
        setText: function(txt) {
            let text = this.$elem.find('.text');
            if(text.length)
            {
                text.text(txt);
            }
            return this.$elem;
        },
        destroy: function() {
            this.$elem.removeData('plugin.preloader');
            this.$elem.find('> ._xlabs_preloader').remove();
            this.config.onDestroy();
            return this.$elem;
        }
    };

    preloader.defaults = preloader.prototype.defaults;

    $.fn.preloader = function(options = {}) {
        options.index = preloader_instances;
        preloader_instances++;
        return $(this).data('plugin.preloader') ? $(this).data('plugin.preloader') : new preloader(this, options);
        //return this.each(function() {});
    };

    window.preloader = preloader;
})(window, jQuery);