<?php

namespace XLabs\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use \DateTime;
use XLabs\ForumBundle\Entity\Topic;
use Symfony\Component\Validator\Constraints as Assert;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 * @ORM\Entity(repositoryClass="XLabs\ForumBundle\Repository\PostRepository")
 * @ORM\Table(name="xlabs_forum_posts")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem"),
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="prefix", method="getXLabsResultCacheKeyForCollection")
 * })
 */
class Post
{
    const IMAGES_FOLDER = 'posts';
    // Keys used in PostRepository
    const RESULT_CACHE_ITEM_PREFIX = 'xlabs_forum_post_';
    const RESULT_CACHE_ITEM_TTL = 604800; // 1 week
    // Keys used in SearchPost
    const RESULT_CACHE_COLLECTION_PREFIX = 'xlabs_forum_posts_';
    const RESULT_CACHE_COLLECTION_TTL = 3600;

    const STATUS_APPROVED = 0;
    const STATUS_WAITING_FOR_APPROVAL = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="title", type="string", length=100, nullable=true, options={"collation": "utf8mb4_unicode_ci"})
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "The title cannot be longer than {{ limit }} characters"
     * )
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @ORM\Column(name="body", type="text", nullable=true, options={"collation": "utf8mb4_unicode_ci"})
     */
    private $body;

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @ORM\Column(name="image", type="string", length=128, nullable=true)
     */
    private $image;

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @ORM\Column(name="folder", type="string", length=255, nullable=true)
     */
    protected $folder;

    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @ORM\Column(name="creationdate", type="datetime", nullable=false)
     */
    private $creationdate;

    public function getCreationdate()
    {
        return $this->creationdate;
    }

    public function setCreationdate($creationdate)
    {
        $this->creationdate = $creationdate;
    }

    /**
     * @ORM\Column(name="publishdate", type="datetime", nullable=true)
     */
    private $publishdate;

    public function getPublishdate()
    {
        return $this->publishdate;
    }

    public function setPublishdate($publishdate)
    {
        if($publishdate instanceof DateTime)
        {
            $publishdate = $publishdate->format('d-m-Y H:i:s');
        }
        if(is_array($publishdate) && array_key_exists('publishdate_date', $publishdate)) // patch for the compound form data upon 'PATCH' request
        {
            $publishdate = $publishdate['publishdate_date'];
        }
        if($publishdate)
        {
            // Patch for disabled datepicker
            $this->publishdate = DateTime::createFromFormat('d-m-Y H:i:s', $publishdate);
        }
    }

    /*
     * @ORM\Column(name="content_type", type="smallint", nullable=false)
     */
    /*private $content_type;

    public function getContentType()
    {
        return $this->content_type;
    }

    public function setContentType($content_type)
    {
        $this->content_type = $content_type;
    }*/

    /*
     * @ORM\Column(name="visibility", type="smallint", nullable=false)
     */
    /*private $visibility;

    public function getVisibility()
    {
        return $this->visibility;
    }

    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;
    }*/

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     */
    private $hidden;

    public function getHidden()
    {
        return $this->hidden;
    }

    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    public function isHidden()
    {
        return $this->getHidden();
    }

    /*
     * Unmapped, for admin form purposes
     */
    protected $preserve_html;

    public function getPreserveHTML()
    {
        return $this->preserve_html;
    }

    public function setPreserveHTML($preserve_html)
    {
        $this->preserve_html = $preserve_html;
    }

    /*
     * @ORM\Column(name="depth", type="integer", nullable=false)
     */
    /*protected $depth = 0;

    public function getDepth()
    {
        return $this->depth;
    }

    public function setDepth($depth)
    {
        $this->depth = $depth;
    }*/

    /*
     * @ORM\Column(name="ancestors", type="text", nullable=true)
     */
    /*protected $ancestors;

    public function getAncestors()
    {
        return $this->ancestors ? array_filter(explode('/', $this->ancestors)) : array();
    }

    public function setAncestors(array $ancestors)
    {
        $this->ancestors = '/'.implode('/', $ancestors).'/';
        $this->depth = count($ancestors);
    }

    public function addAncestor($ancestor_id)
    {
        $ancestors = $this->getAncestors();
        $ancestors[] = $ancestor_id;
        $this->setAncestors($ancestors);
    }*/

    /**
     * @ORM\ManyToOne(targetEntity="XLabs\ForumBundle\Entity\Post", inversedBy="replies")
     * @ORM\JoinColumn(name="thread_id", referencedColumnName="id")
     */
    private $thread;

    public function getThread()
    {
        return $this->thread;
    }

    public function setThread($thread)
    {
        $this->thread = $thread;
    }

    /**
     * @ORM\OneToMany(targetEntity="XLabs\ForumBundle\Entity\Post", mappedBy="thread", orphanRemoval=true)
     */
    private $replies;

    /*
     * Mapped through event listener
     */
    protected $author;

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @ORM\ManyToOne(targetEntity="XLabs\ForumBundle\Entity\Topic", inversedBy="posts")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id")
     **/
    private $topic;

    public function getTopic()
    {
        return $this->topic;
    }

    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @ORM\ManyToOne(targetEntity="XLabs\ForumBundle\Entity\Post")
     * @ORM\JoinColumn(name="quoted_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $quoted;

    public function getQuoted()
    {
        return $this->quoted;
    }

    public function setQuoted($quoted)
    {
        $this->quoted = $quoted;
    }

    public function __construct()
    {
        $this->creationdate = new DateTime();
        $this->publishdate = new DateTime();
        $this->hidden = false;
        $this->folder = $this->creationdate->format('dmYHis');
        $this->replies = new ArrayCollection();
        $this->preserve_html = false;
    }

    public function __get($name)
    {
        return $this->$name;
    }
    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    public function getXLabsResultCacheKeyForItem()
    {
        return array(
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId(),
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId().'_hydration',
        );
    }

    public function getXLabsResultCacheKeyForCollection()
    {
        $keys = array(
            $this::RESULT_CACHE_COLLECTION_PREFIX,
            Topic::RESULT_CACHE_COLLECTION_PREFIX,
        );
        return $keys;
    }
}