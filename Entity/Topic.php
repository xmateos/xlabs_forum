<?php

namespace XLabs\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use XLabs\ForumBundle\Helpers\Canonicalizer;
use \DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use XLabs\ForumBundle\Entity\Category;
use XLabs\ForumBundle\Entity\Post;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 * @ORM\Entity(repositoryClass="XLabs\ForumBundle\Repository\TopicRepository")
 * @ORM\Table(name="xlabs_forum_topics")
 * UniqueEntity(fields={"title"}, message="This topic already exists.")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem"),
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="prefix", method="getXLabsResultCacheKeyForCollection"),
 * })
 */
class Topic
{
    const COVERS_FOLDER = 'topics';
    const RESULT_CACHE_ITEM_PREFIX = 'xlabs_forum_topic_';
    const RESULT_CACHE_ITEM_TTL = 86400;
    const RESULT_CACHE_COLLECTION_PREFIX = 'xlabs_forum_topic_';
    const RESULT_CACHE_COLLECTION_TTL = 3600;

    const STATUS_APPROVED = 0;
    const STATUS_WAITING_FOR_APPROVAL = 1;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * ORM\Column(name="title", type="string", length=50, nullable=false, unique=true)
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     */
    private $title;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        $this->setCanonical(Canonicalizer::canonicalize(strip_tags(html_entity_decode($this->title),null)));
    }

    /**
     * @ORM\Column(name="canonical", type="string", length=50, nullable=false)
     */
    private $canonical;

    public function getCanonical()
    {
        return $this->canonical;
    }

    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;
    }

    /**
     * @ORM\Column(name="body", type="text", nullable=true, options={"collation": "utf8mb4_unicode_ci"})
     */
    private $body;

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @ORM\Column(name="cover", type="string", length=128, nullable=true)
     */
    private $cover;

    public function getCover()
    {
        return $this->cover;
    }

    public function setCover($cover)
    {
        $this->cover = $cover;
        return $this;
    }

    /**
     * @ORM\Column(name="folder", type="string", length=255, nullable=true)
     */
    protected $folder;

    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @ORM\Column(name="creationdate", type="datetime", nullable=false)
     */
    private $creationdate;

    public function getCreationdate()
    {
        return $this->creationdate;
    }

    public function setCreationdate($creationdate)
    {
        $this->creationdate = $creationdate;
    }

    /**
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     */
    private $hidden;

    public function getHidden()
    {
        return $this->hidden;
    }

    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @ORM\Column(name="pinned", type="boolean", nullable=false)
     */
    private $pinned;

    public function getPinned()
    {
        return $this->pinned;
    }

    public function setPinned($pinned)
    {
        $this->pinned = $pinned;
    }

    /*
     * Unmapped, for admin form purposes
     */
    protected $preserve_html;

    public function getPreserveHTML()
    {
        return $this->preserve_html;
    }

    public function setPreserveHTML($preserve_html)
    {
        $this->preserve_html = $preserve_html;
    }

    /*
     * Mapped through event listener
     */
    protected $author;

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @ORM\ManyToOne(targetEntity="XLabs\ForumBundle\Entity\Category", inversedBy="topics")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="SET NULL")
     **/
    private $category;

    public function getCategory()
    {
        return $this->category;
    }

    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @ORM\OneToMany(targetEntity="XLabs\ForumBundle\Entity\Post", mappedBy="topic", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $posts;

    public function getPosts()
    {
        return $this->posts;
    }

    public function addPost(Post $post)
    {
        $this->posts->add($post);
        $post->setTopic($this);
    }

    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    public function __construct()
    {
        $this->creationdate = new DateTime();
        $this->hidden = false;
        $this->posts = new ArrayCollection();
        $this->folder = $this->creationdate->format('dmYHis');
        $this->pinned = false;
        $this->preserve_html = false;
    }

    public function __toString()
    {
        return $this->getTitle();
    }

    public function getXLabsResultCacheKeyForItem()
    {
        $keys = array(
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId(),
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId().'_hydration'
        );
        if($this->getCategory())
        {
            $keys[] = Category::RESULT_CACHE_ITEM_PREFIX.$this->getCategory()->getId();
            $keys[] = Category::RESULT_CACHE_ITEM_PREFIX.$this->getCategory()->getId().'_hydration';
        }
        return $keys;
    }

    public function getXLabsResultCacheKeyForCollection()
    {
        return $this::RESULT_CACHE_COLLECTION_PREFIX;
    }
}