<?php

namespace XLabs\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use XLabs\ForumBundle\Helpers\Canonicalizer;
use \DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use XLabs\ForumBundle\Entity\Topic;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 * @ORM\Entity(repositoryClass="XLabs\ForumBundle\Repository\CategoryRepository")
 * @ORM\Table(name="xlabs_forum_categories")
 * @UniqueEntity(fields={"name"}, message="This category already exists.")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="literal", method="getXLabsResultCacheKeyForItem"),
 *      @XLabsResultCache\Key(onFlush={"insert", "update", "delete"}, type="prefix", method="getXLabsResultCacheKeyForCollection"),
 * })
 */
class Category
{
    const COVERS_FOLDER = 'categories';
    const RESULT_CACHE_ITEM_PREFIX = 'xlabs_forum_category_';
    const RESULT_CACHE_ITEM_TTL = 86400;
    const RESULT_CACHE_COLLECTION_PREFIX = 'xlabs_forum_categories_';
    const RESULT_CACHE_COLLECTION_TTL = 3600;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(name="name", type="string", length=50, nullable=false, unique=true)
     */
    private $name;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        $this->setCanonical(Canonicalizer::canonicalize($name));
    }

    /**
     * @ORM\Column(name="canonical", type="string", length=50, nullable=false)
     */
    private $canonical;

    public function getCanonical()
    {
        return $this->canonical;
    }

    public function setCanonical($canonical)
    {
        $this->canonical = $canonical;
    }

    /**
     * @ORM\Column(name="cover", type="string", length=128, nullable=true)
     */
    private $cover;

    public function getCover()
    {
        return $this->cover;
    }

    public function setCover($cover)
    {
        $this->cover = $cover;
        return $this;
    }

    /**
     * @ORM\Column(name="folder", type="string", length=255, nullable=true)
     */
    protected $folder;

    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @ORM\Column(name="creationdate", type="datetime", nullable=false)
     */
    private $creationdate;

    public function getCreationdate()
    {
        return $this->creationdate;
    }

    public function setCreationdate($creationdate)
    {
        $this->creationdate = $creationdate;
    }

    /**
     * @ORM\Column(name="hidden", type="boolean", nullable=false)
     */
    private $hidden;

    public function getHidden()
    {
        return $this->hidden;
    }

    public function setHidden($hidden)
    {
        $this->hidden = $hidden;
    }

    /**
     * ORM\OneToMany(targetEntity="XLabs\ForumBundle\Entity\Topic", mappedBy="category", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OneToMany(targetEntity="XLabs\ForumBundle\Entity\Topic", mappedBy="category", cascade={"persist"})
     */
    protected $topics;

    public function getTopics()
    {
        return $this->topics;
    }

    public function addTopic(Topic $topic)
    {
        $this->topics->add($topic);
        $topic->setCategory($this);
    }

    public function removeTopic(Topic $topic)
    {
        $this->topics->removeElement($topic);
    }

    public function __construct()
    {
        $this->creationdate = new DateTime();
        $this->hidden = false;
        $this->topics = new ArrayCollection();
        $this->folder = $this->creationdate->format('dmYHis');
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getXLabsResultCacheKeyForItem()
    {
        return array(
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId(),
            $this::RESULT_CACHE_ITEM_PREFIX.$this->getId().'_hydration'
        );
    }

    public function getXLabsResultCacheKeyForCollection()
    {
        return $this::RESULT_CACHE_COLLECTION_PREFIX;
    }
}