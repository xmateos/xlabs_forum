<?php

namespace XLabs\ForumBundle\Event\Post;

use Symfony\Component\EventDispatcher\Event;

class OnEdit extends Event
{
    const NAME = 'xlabs.forum.post.edit.event';

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}