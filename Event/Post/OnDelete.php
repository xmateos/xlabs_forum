<?php

namespace XLabs\ForumBundle\Event\Post;

use Symfony\Component\EventDispatcher\Event;

class OnDelete extends Event
{
    const NAME = 'xlabs.forum.post.delete.event';

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}