<?php

namespace XLabs\ForumBundle\Event\Topic;

use Symfony\Component\EventDispatcher\Event;

class OnApproved extends Event
{
    const NAME = 'xlabs.forum.topic.approved.event';

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }
}