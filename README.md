A forum bundle.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/forumbundle
```

This bundle depends on "xlabs/forumbundle". Make sure to set it up too.

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\ForumBundle\XLabsForumBundle(),
    ];
}
```

## Requirements

```php
# app/config.yml

doctrine:
    ...
    orm:
        ...
        resolve_target_entities:
            ...
            XLabs\ForumBundle\Model\UserInterface: YourBundle\Entity\YourFOSUserExtendedEntity

assetic:
    ...
    bundles: [ ..., 'XLabsForumBundle']
    ...
```

```bash
php bin/console doctrine:schema:update --force
php bin/console fos:js-routing:dump && php bin/console assetic:dump && php bin/console assetic:dump --env=prod
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
x_labs_forum:
    resource: .
    type: xlabs_forum_routing
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
x_labs_forum:
    url_prefix: ''
    forum_admin_index: '/admin/index'
    forum_index: '/index'
    topic_index: '/topic'
    thread_index: '/thread'
    user_entity: My/FOSUser/Entity
    admin_user: 'myadminuser' # the one to create items from the admin
    use_emojis: true
    uploads:
        folder: media/forum
        allowed_extensions: ['jpg', 'jpeg', 'gif', 'png']
        max_file_size: 1048576
```