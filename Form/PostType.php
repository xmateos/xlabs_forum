<?php

namespace XLabs\ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use XLabs\ForumBundle\Entity\Topic;
use XLabs\ForumBundle\Entity\Post;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use XLabs\ForumBundle\Form\CustomFormFields\SingleImageUploadType;
use Symfony\Component\Validator\Constraints\Image as ImageConstraint;
use XLabs\ForumBundle\Form\CustomFormFields\EmojiTextInputType;
use XLabs\ForumBundle\Form\CustomFormFields\EmojiTextareaInputType;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $xlabs_forum_config = $options['xlabs_forum_config'];
        $post = $options['data'];
        $builder
            ->add('author', TextType::class, array(
                'mapped' => false,
                'data' => $post->getAuthor()->getUsername(),
                //'placeholder' => $post->getAuthor()->getUsername(),
                'attr' => array(
                    'readonly' => true,
                ),
            ))
            ->add('title', EmojiTextInputType::class, array(
                'required' => $post->getThread() ? false : true, // only threads require a title
            ))
            ->add('body', EmojiTextareaInputType::class, array(
                'required' => true,
            ))
            /*->add('publishdate', DateTimeType::class, array(
                'required' => false
            ))*/
            ->add('hidden',CheckboxType::class, array(
                'required' => false
            ))
            ->add('status', ChoiceType::class, array(
                'choices' => array_flip(array(
                    '' => 'Select ...',
                    Post::STATUS_APPROVED => 'Approved',
                    Post::STATUS_WAITING_FOR_APPROVAL => 'Waiting for approval'
                )),
                'multiple' => false,
                'required' => true,
                'expanded' => false,
                'attr' => array(
                    //'class' => 'hidden-field'
                )
            ))
            ->add('topic', EntityType::class, array(
                'class' => Topic::class,
                'query_builder' => function(EntityRepository $er) {
                    $qb = $er->createQueryBuilder('e');
                    return $qb
                        ->orderBy('e.title', 'ASC');
                },
                'required' => true,
                'multiple' => false,
                'placeholder' => 'Select ...',
                'empty_data'  => null,
                'expanded' => false
            ))
            ->add('image', SingleImageUploadType::class, array(
                'required' => false,
                'constraints' => array(
                    new ImageConstraint(array(
                        'maxSize' => $xlabs_forum_config['uploads']['max_file_size']
                    ))
                )
            ))
            ->add('preserve_html',CheckboxType::class, array(
                'required' => false,
                'label' => 'Preserve html tags in title/body'
            ))
            ->add('save', SubmitType::class, array(
                'attr' => array(
                    'class' => 'button _form_but _form_but_save',
                    'disabled' => false
                ),
                'label' => '<i class="fas fa-save"></i>Save'
            ))
            ->add('cancel', ButtonType::class, array(
                'attr' => array(
                    'class' => 'button red _form_but _form_but_cancel'
                ),
                'label' => '<i class="fas fa-ban"></i>Cancel'
            ))
        ;

        $factory = $builder->getFormFactory();
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use($factory){
            $form = $event->getForm();
            $post = $event->getData();

            if($post->getThread())
            {
                $form->remove('title');
                $form->remove('topic');
            }
        });
    }

    public function getBlockPrefix()
    {
        return 'xlabs_forumbundle_posttype';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Post::class,
        ));
        $resolver->setRequired('xlabs_forum_config');
    }
}
