<?php

namespace XLabs\ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use XLabs\ForumBundle\Entity\Topic;
use XLabs\ForumBundle\Entity\Category;
use Doctrine\ORM\EntityRepository;
use XLabs\ForumBundle\Form\CustomFormFields\SingleImageUploadType;
use Symfony\Component\Validator\Constraints\Image as ImageConstraint;
use XLabs\ForumBundle\Form\CustomFormFields\EmojiTextareaInputType;

class TopicType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $xlabs_forum_config = $options['xlabs_forum_config'];
        $topic = $options['data'];
        $builder
            ->add('category', EntityType::class, array(
                'class' => Category::class,
                'query_builder' => function(EntityRepository $er) {
                    $qb = $er->createQueryBuilder('e');
                    return $qb
                        ->orderBy('e.name', 'ASC');
                },
                'required' => false,
                'multiple' => false,
                'placeholder' => 'Select ...',
                'empty_data'  => null,
                'expanded' => false
            ))
            ->add('title', TextType::class, array(
                'required' => true,
                'label' => '',
                'attr' => array(
                    'autocomplete' => 'off'
                )
            ))
            ->add('body', EmojiTextareaInputType::class, array(
                'required' => false
            ))
            ->add('hidden',CheckboxType::class, array(
                'required' => false
            ))
            ->add('pinned',CheckboxType::class, array(
                'required' => false
            ))
            ->add('status', ChoiceType::class, array(
                'choices' => array_flip(array(
                    '' => 'Select ...',
                    Topic::STATUS_APPROVED => 'Approved',
                    Topic::STATUS_WAITING_FOR_APPROVAL => 'Waiting for approval'
                )),
                'multiple' => false,
                //'placeholder' => '',
                //'empty_data' => $topic->getStatus() ? $topic->getStatus() : Topic::STATUS_WAITING_FOR_APPROVAL,
                'required' => true,
                'expanded' => false,
                'attr' => array(
                    //'class' => 'hidden-field'
                )
            ))
            ->add('cover', SingleImageUploadType::class, array(
                'required' => false,
                'constraints' => array(
                    new ImageConstraint(array(
                        'maxSize' => $xlabs_forum_config['uploads']['max_file_size']
                    ))
                )
            ))
            ->add('preserve_html',CheckboxType::class, array(
                'required' => false,
                'label' => 'Preserve html tags in title/body'
            ))
            ->add('save', SubmitType::class, array(
                'attr' => array(
                    'class' => 'button _form_but _form_but_save',
                    'disabled' => false
                ),
                'label' => '<i class="fas fa-save"></i>Save'
            ))
            ->add('cancel', ButtonType::class, array(
                'attr' => array(
                    'class' => 'button red _form_but _form_but_cancel'
                ),
                'label' => '<i class="fas fa-ban"></i>Cancel'
            ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'xlabs_forumbundle_topictype';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Topic::class,
        ));
        $resolver->setRequired('xlabs_forum_config');
    }
}
