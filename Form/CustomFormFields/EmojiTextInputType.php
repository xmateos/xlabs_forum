<?php

namespace XLabs\ForumBundle\Form\CustomFormFields;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class EmojiTextInputType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'required' => false,
            'label' => '',
            'attr' => array(
                'autocomplete' => 'off'
            )
        ));
    }

    public function getParent()
    {
        return TextType::class;
    }

    public function getBlockPrefix()
    {
        return 'xlabs_forum_emoji_text_input';
    }
}