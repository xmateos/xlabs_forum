<?php

namespace XLabs\ForumBundle\Form\CustomFormFields\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SingleImageUploadTransformer implements DataTransformerInterface
{
    private $options;
    private $fieldname;

    public function __construct($options, $fieldname)
    {
        $this->options = $options;
        $this->fieldname = $fieldname;
    }

    /*
     * Transforms an object to a string
     */
    public function transform($val)
    {
        return array(
            $this->fieldname => $val,
            $this->fieldname.'_file' => null
        );
    }

    /*
     * Transforms a string to an object.
     */
    public function reverseTransform($val)
    {
        if($val[$this->fieldname.'_file'] instanceof UploadedFile)
        {
            return $val[$this->fieldname.'_file'];
        } else {
            return $val[$this->fieldname];
        }
    }
}