<?php

namespace XLabs\ForumBundle\Form\CustomFormFields;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use XLabs\ForumBundle\Form\CustomFormFields\DataTransformer\SingleImageUploadTransformer;

class SingleImageUploadType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'compound' => true,
            'required' => false,
            //'data_class' => null
        ));
        $resolver->setDefined('constraints');
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) use ($options){
            $form = $event->getForm();
            $data = $event->getData();
            $form
                ->add($form->getName(), HiddenType::class, array(
                    'required' => false
                ))
                ->add($form->getName().'_file', FileType::class, array(
                    'required' => $options['required'],
                    'multiple' => false,
                    'data_class' => null,
                    'data' => null,
                    //'mapped' => false,
                    'constraints' => $options['constraints']
                ))
            ;
        });
        $transformer = new SingleImageUploadTransformer($options, $builder->getName());
        $builder->addModelTransformer($transformer);
    }

    public function getParent()
    {
        return HiddenType::class;
    }

    public function getBlockPrefix()
    {
        return 'xlabs_forum_single_image_upload';
    }
}