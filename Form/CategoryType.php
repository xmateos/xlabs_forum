<?php

namespace XLabs\ForumBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use XLabs\ForumBundle\Entity\Category;
use XLabs\ForumBundle\Form\CustomFormFields\SingleImageUploadType;
use Symfony\Component\Validator\Constraints\Image as ImageConstraint;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $xlabs_forum_config = $options['xlabs_forum_config'];
        $builder
            ->add('name', TextType::class, array(
                'required' => true,
                'label' => '',
                'attr' => array(
                    'autocomplete' => 'off'
                )
            ))
            ->add('cover', SingleImageUploadType::class, array(
                'required' => false,
                'constraints' => array(
                    new ImageConstraint(array(
                        'maxSize' => $xlabs_forum_config['uploads']['max_file_size']
                    ))
                )
            ))
            ->add('hidden',CheckboxType::class, array(
                'required' => false,
                'label' => 'Mark as hidden'
            ))
            ->add('save', SubmitType::class, array(
                'attr' => array(
                    'class' => 'button _form_but _form_but_save',
                    'disabled' => false
                ),
                'label' => '<i class="fas fa-save"></i>Save'
            ))
            ->add('cancel', ButtonType::class, array(
                'attr' => array(
                    'class' => 'button red _form_but _form_but_cancel'
                ),
                'label' => '<i class="fas fa-ban"></i>Cancel'
            ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'xlabs_forumbundle_categorytype';
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Category::class,
        ));
        $resolver->setRequired('xlabs_forum_config');
    }
}
