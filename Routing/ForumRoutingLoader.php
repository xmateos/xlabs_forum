<?php

namespace XLabs\ForumBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use \DateTime;

class ForumRoutingLoader extends Loader
{
    private $config;
    private $loaded = false;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function load($resource, $type = null)
    {
        if (true === $this->loaded) {
            throw new \RuntimeException('Do not add the "xlabs_forum_routing" loader twice');
        }

        $routes = new RouteCollection();
        $aRoutes = array();

        // Forum URL prefix
        $path_prefix = $this->config['url_prefix'];
        $path_prefix = $path_prefix ? $path_prefix : '';

        // Admin index
        $path = $path_prefix.$this->config['forum_admin_index'];
        $defaults = array(
            '_controller' => 'XLabsForumBundle:ForumAdmin:index',
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_forum_admin_index';
        $aRoutes[$routeName] = $route;

        // Admin results search/pagination
        $path = $path_prefix.$this->config['forum_admin_load_results'];
        $defaults = array(
            '_controller' => 'XLabsForumBundle:ForumAdmin:renderResults',
        );
        $requirements = array();
        $options = array(
            'expose' => 'true'
        );
        $route = new Route($path, $defaults, $requirements, $options);
        $routeName = 'xlabs_forum_admin_load_results';
        $aRoutes[$routeName] = $route;

        // Index (topics)
        $path = $path_prefix.$this->config['forum_index'];
        $defaults = array(
            '_controller' => 'XLabsForumBundle:Forum:index',
        );
        $requirements = array();
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_forum_index';
        $aRoutes[$routeName] = $route;

        // Topic index
        $path = $path_prefix.$this->config['topic_index'].'/{topic_id}';
        $defaults = array(
            '_controller' => 'XLabsForumBundle:Forum:topic',
        );
        $requirements = array(
            'topic_id' => '\d+'
        );
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_forum_topic_index';
        $aRoutes[$routeName] = $route;

        // Thread index
        $path = $path_prefix.$this->config['thread_index'].'/{thread_id}';
        $defaults = array(
            '_controller' => 'XLabsForumBundle:Forum:thread',
        );
        $requirements = array(
            'thread_id' => '\d+'
        );
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_forum_thread_index';
        $aRoutes[$routeName] = $route;

        // Reply
        /*$path = $path_prefix.$this->config['thread_index'].'/{thread_id}/reply';
        $defaults = array(
            '_controller' => 'XLabsForumBundle:Forum:reply',
        );
        $requirements = array(
            'thread_id' => '\d+'
        );
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_forum_thread_reply';
        $aRoutes[$routeName] = $route;*/

        // Quote
        $path = $path_prefix.$this->config['thread_index'].'/{thread_id}/quote/{post_id}';
        $defaults = array(
            '_controller' => 'XLabsForumBundle:Forum:quote',
        );
        $requirements = array(
            'thread_id' => '\d+',
            'post_id' => '\d+'
        );
        $route = new Route($path, $defaults, $requirements);
        $routeName = 'xlabs_forum_post_quote';
        $aRoutes[$routeName] = $route;

        foreach($aRoutes as $routeName => $route)
        {
            $routes->add($routeName, $route);
        }

        // Add other bundle routing files
        //$resource = '@ForumBundle/Resources/config/routing.yml';
        $resource = __DIR__.'/../Resources/config/routing.yml';
        $type = 'yaml';
        $importedRoutes = $this->import($resource, $type);
        $routes->addCollection($importedRoutes);

        $this->loaded = true;

        return $routes;
    }

    public function supports($resource, $type = null)
    {
        return 'xlabs_forum_routing' === $type;
    }
}