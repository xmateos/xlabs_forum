<?php

namespace XLabs\ForumBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use XLabs\ForumBundle\Entity\Category;
use XLabs\ForumBundle\Entity\Topic;
use XLabs\ForumBundle\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\ForumBundle\Services\FileUpload;
use Symfony\Component\Filesystem\Filesystem;

class CategoryListener
{
    private $em;
    private $file_upload;
    private $kernel_rootdir;
    private $config;

    public function __construct(EntityManagerInterface $em, FileUpload $file_upload, $kernel_rootdir, $config)
    {
        $this->em = $em;
        $this->file_upload = $file_upload;
        $this->kernel_rootdir = $kernel_rootdir;
        $this->config = $config;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Category)
        {
            return;
        }

        $destination_folder = $this->config['uploads']['folder'].'/'.Category::COVERS_FOLDER.'/'.$e->getFolder().'/';
        if($e->getCover() instanceof UploadedFile)
        {
            $filename = $this->file_upload->upload($e->getCover(), $destination_folder);
            if($filename)
            {
                $e->setCover($filename);
            }
        }
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Category)
        {
            return;
        }

        $destination_folder = $this->config['uploads']['folder'].'/'.Category::COVERS_FOLDER.'/'.$e->getFolder().'/';
        if($args->hasChangedField('cover'))
        {
            $oldValue = $args->getOldValue('cover');
            $newValue = $args->getNewValue('cover');

            $filename = $this->file_upload->upload($newValue, $destination_folder);
            if($filename || is_null($filename))
            {
                $e->setCover($filename);
                @unlink($oldValue);

                // clear folder if image deleted
                if(is_null($newValue))
                {
                    $web_folder = $this->kernel_rootdir.'/../web/';
                    $folder = $web_folder.$this->config['uploads']['folder'].'/'.Category::COVERS_FOLDER.'/'.$e->getFolder();
                    $filesystem = new Filesystem();
                    $filesystem->remove(array($folder));
                }
            } else {
                // error, image not uploaded
                $e->setCover($oldValue);
            }
        }

        $this->clearCacheForAssociations($e);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Category)
        {
            return;
        }
        $filename = $e->getCover();
        if($filename)
        {
            $e->setCover(null);
            @unlink($filename);

            // clear folder
            $web_folder = $this->kernel_rootdir.'/../web/';
            $folder = $web_folder.$this->config['uploads']['folder'].'/'.Category::COVERS_FOLDER.'/'.$e->getFolder();
            $filesystem = new Filesystem();
            $filesystem->remove(array($folder));
        }

        $cache = $this->em->getConfiguration()->getResultCacheImpl();
        // unassign category in all its associated topics
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('t.id')
            ->from(Topic::class, 't')
            ->join('t.category', 't_c')
            ->where(
                $qb->expr()->eq('t_c.id', $e->getId())
            );
        $topics = $qb->getQuery()->getArrayResult();
        foreach($topics as $topic)
        {
            $cache->delete(Topic::RESULT_CACHE_ITEM_PREFIX.$topic['id']);
            $cache->delete(Topic::RESULT_CACHE_ITEM_PREFIX.$topic['id'].'_hydration');
        }
    }

    private function clearCacheForAssociations($category)
    {
        $cache = $this->em->getConfiguration()->getResultCacheImpl();

        // Topics
        if($category instanceof Category && $category->getId())
        {
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('t.id')
                ->from(Topic::class, 't')
                ->join('t.category', 't_c')
                ->where(
                    $qb->expr()->eq('t_c.id', $category->getId())
                );
            $topics = $qb->getQuery()->getArrayResult();
            foreach($topics as $topic)
            {
                $cache->delete(Topic::RESULT_CACHE_ITEM_PREFIX.$topic['id']);
                $cache->delete(Topic::RESULT_CACHE_ITEM_PREFIX.$topic['id'].'_hydration');

                // Posts (it could be really intense)
                $qb = $this->em->createQueryBuilder();
                $qb
                    ->select('p.id')
                    ->from(Post::class, 'p')
                    ->join('p.topic', 'p_t')
                    ->where(
                        $qb->expr()->eq('p_t.id', $topic['id'])
                    );
                $posts = $qb->getQuery()->getArrayResult();
                foreach($posts as $post)
                {
                    $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post['id']);
                    $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post['id'].'_hydration');
                }
            }
        }
    }
}