<?php

namespace XLabs\ForumBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use XLabs\ForumBundle\Entity\Category;
use XLabs\ForumBundle\Entity\Topic;
use XLabs\ForumBundle\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\ForumBundle\Services\FileUpload;
use Symfony\Component\Filesystem\Filesystem;

class TopicListener
{
    private $em;
    private $file_upload;
    private $kernel_rootdir;
    private $config;

    public function __construct(EntityManagerInterface $em, FileUpload $file_upload, $kernel_rootdir, $config)
    {
        $this->em = $em;
        $this->file_upload = $file_upload;
        $this->kernel_rootdir = $kernel_rootdir;
        $this->config = $config;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Topic)
        {
            return;
        }

        if(!$e->getPreserveHTML())
        {
            $e->setTitle(nl2br(strip_tags(html_entity_decode($e->getTitle()),null)));
            $e->setBody(nl2br(strip_tags(html_entity_decode($e->getBody()),'<span>')));
        }

        $destination_folder = $this->config['uploads']['folder'].'/'.Topic::COVERS_FOLDER.'/'.$e->getFolder().'/';
        if($e->getCover() instanceof UploadedFile)
        {
            $filename = $this->file_upload->upload($e->getCover(), $destination_folder);
            if($filename)
            {
                $e->setCover($filename);
            }
        }

        $this->clearCacheForAssociations($e, $e->getCategory());
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Topic)
        {
            return;
        }

        if($args->hasChangedField('title'))
        {
            if(!$e->getPreserveHTML())
            {
                $e->setTitle(nl2br(strip_tags(html_entity_decode($e->getTitle()),null)));
            }
        }

        if($args->hasChangedField('body'))
        {
            if(!$e->getPreserveHTML())
            {
                $e->setBody(nl2br(strip_tags(html_entity_decode($e->getBody()),'<span>')));
            }
        }

        if($args->hasChangedField('category'))
        {
            $oldValue = $args->getOldValue('category');
            $this->clearCacheForAssociations($e, $oldValue);

            $newValue = $args->getNewValue('category');
            //$this->clearCacheForAssociations($e, $newValue); // it gets cleared anyways

            /*$cache = $this->em->getConfiguration()->getResultCacheImpl();
            if($oldValue instanceof Category)
            {
                $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$oldValue->getId());
                $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$oldValue->getId().'_hydration');
            }
            if($newValue instanceof Category)
            {
                $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$newValue->getId());
                $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$newValue->getId().'_hydration');
            }*/
        }

        $destination_folder = $this->config['uploads']['folder'].'/'.Topic::COVERS_FOLDER.'/'.$e->getFolder().'/';
        if($args->hasChangedField('cover'))
        {
            $oldValue = $args->getOldValue('cover');
            $newValue = $args->getNewValue('cover');

            $filename = $this->file_upload->upload($newValue, $destination_folder);
            if($filename || is_null($filename))
            {
                $e->setCover($filename);
                @unlink($oldValue);

                // clear folder if image deleted
                if(is_null($newValue))
                {
                    $web_folder = $this->kernel_rootdir.'/../web/';
                    $folder = $web_folder.$this->config['uploads']['folder'].'/'.Topic::COVERS_FOLDER.'/'.$e->getFolder();
                    $filesystem = new Filesystem();
                    $filesystem->remove(array($folder));
                }
            } else {
                // error, image not uploaded
                $e->setCover($oldValue);
            }
        }

        $this->clearCacheForAssociations($e, $e->getCategory());

        // clear cache for its posts
        /*$cache = $this->em->getConfiguration()->getResultCacheImpl();
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('p.id')
            ->from(Post::class, 'p')
            ->join('p.topic', 'p_t')
            ->where(
                $qb->expr()->eq('p_t.id', $e->getId())
            );
        $posts = $qb->getQuery()->getArrayResult();
        foreach($posts as $post)
        {
            $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post['id']);
            $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post['id'].'_hydration');
        }*/
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Topic)
        {
            return;
        }
        $filename = $e->getCover();
        if($filename)
        {
            $e->setCover(null);
            @unlink($filename);

            // clear folder
            $web_folder = $this->kernel_rootdir.'/../web/';
            $folder = $web_folder.$this->config['uploads']['folder'].'/'.Topic::COVERS_FOLDER.'/'.$e->getFolder();
            $filesystem = new Filesystem();
            $filesystem->remove(array($folder));
        }

        $this->clearCacheForAssociations($e, $e->getCategory());
        /*$cache = $this->em->getConfiguration()->getResultCacheImpl();
        if($e->getCategory())
        {
            $category_id = $e->getCategory();
            $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$category_id);
            $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$category_id.'_hydration');
        }

        // unassign category in all its associated topics
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('p.id')
            ->from(Post::class, 'p')
            ->join('p.topic', 'p_t')
            ->where(
                $qb->expr()->eq('p_t.id', $e->getId())
            );
        $posts = $qb->getQuery()->getArrayResult();
        foreach($posts as $post)
        {
            $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post['id']);
            $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post['id'].'_hydration');
        }*/
    }

    private function clearCacheForAssociations($topic, $category)
    {
        $cache = $this->em->getConfiguration()->getResultCacheImpl();

        // Category
        if($category instanceof Category && $category->getId())
        {
            $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$category->getId());
            $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$category->getId().'_hydration');
        }

        // Posts
        if($topic instanceof Topic && $topic->getId())
        {
            $qb = $this->em->createQueryBuilder();
            $qb
                ->select('p.id')
                ->from(Post::class, 'p')
                ->join('p.topic', 'p_t')
                ->where(
                    $qb->expr()->eq('p_t.id', $topic->getId())
                );
            $posts = $qb->getQuery()->getArrayResult();
            foreach($posts as $post)
            {
                $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post['id']);
                $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post['id'].'_hydration');
            }
        }
    }
}