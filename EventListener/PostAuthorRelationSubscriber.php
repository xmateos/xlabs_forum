<?php

namespace XLabs\ForumBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;

class PostAuthorRelationSubscriber implements EventSubscriber
{
    const INTERFACE_FQNS = 'XLabs\ForumBundle\Model\UserInterface';
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        /*$targetEntity = false;
        if(in_array(self::INTERFACE_FQNS, class_implements($metadata->getName()))) {
            $targetEntity = $metadata->getName();
        }
        if(!$targetEntity)
        {
            return;
        }*/

        if($metadata->getName() == 'XLabs\ForumBundle\Entity\Post')
        {
            //dump($targetEntity); die;
            $metadata->mapManyToOne(array(
                'targetEntity' => $this->config['user_entity'],
                'fieldName' => 'author',
                'joinColumns' => array(
                    array(
                        'name' => 'author_id',
                        'referencedColumnName' => 'id',
                        'onDelete' => 'SET NULL'
                    )
                )
            ));
        }


    }
}