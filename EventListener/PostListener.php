<?php

namespace XLabs\ForumBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use XLabs\ForumBundle\Entity\Category;
use XLabs\ForumBundle\Entity\Topic;
use XLabs\ForumBundle\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\ForumBundle\Services\FileUpload;
use Symfony\Component\Filesystem\Filesystem;

class PostListener
{
    private $em;
    private $file_upload;
    private $kernel_rootdir;
    private $config;

    public function __construct(EntityManagerInterface $em, FileUpload $file_upload, $kernel_rootdir, $config)
    {
        $this->em = $em;
        $this->file_upload = $file_upload;
        $this->kernel_rootdir = $kernel_rootdir;
        $this->config = $config;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Post)
        {
            return;
        }

        if(!$e->getPreserveHTML())
        {
            $e->setTitle(nl2br(strip_tags(html_entity_decode($e->getTitle()),null)));
            $e->setBody(nl2br(strip_tags(html_entity_decode($e->getBody()),'<span>')));
        }

        $destination_folder = $this->config['uploads']['folder'].'/'.Post::IMAGES_FOLDER.'/'.$e->getFolder().'/';
        if($e->getImage() instanceof UploadedFile)
        {
            $filename = $this->file_upload->upload($e->getImage(), $destination_folder);
            if($filename)
            {
                $e->setImage($filename);
            }
        }

        $this->clearCacheForAssociations($e, $e->getTopic());
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Post)
        {
            return;
        }

        if($args->hasChangedField('title'))
        {
            if(!$e->getPreserveHTML())
            {
                $e->setTitle(nl2br(strip_tags(html_entity_decode($e->getTitle()),null)));
            }
        }

        if($args->hasChangedField('body'))
        {
            if(!$e->getPreserveHTML())
            {
                $e->setBody(nl2br(strip_tags(html_entity_decode($e->getBody()),'<span>')));
            }
        }

        if($args->hasChangedField('topic'))
        {
            $oldValue = $args->getOldValue('topic');
            $this->clearCacheForAssociations($e, $oldValue);

            $newValue = $args->getNewValue('topic');
            //$this->clearCacheForAssociations($newValue); // it gets cleared anyways
        }

        $destination_folder = $this->config['uploads']['folder'].'/'.Post::IMAGES_FOLDER.'/'.$e->getFolder().'/';
        if($args->hasChangedField('image'))
        {
            $oldValue = $args->getOldValue('image');
            $newValue = $args->getNewValue('image');

            $filename = $this->file_upload->upload($newValue, $destination_folder);
            if($filename || is_null($filename))
            {
                $e->setImage($filename);
                @unlink($oldValue);

                // clear folder if image deleted
                if(is_null($newValue))
                {
                    $web_folder = $this->kernel_rootdir.'/../web/';
                    $folder = $web_folder.$this->config['uploads']['folder'].'/'.Post::IMAGES_FOLDER.'/'.$e->getFolder();
                    $filesystem = new Filesystem();
                    $filesystem->remove(array($folder));
                }
            } else {
                // error, image not uploaded
                $e->setImage($oldValue);
            }
        }

        $this->clearCacheForAssociations($e, $e->getTopic());
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $e = $args->getEntity();
        if(!$e instanceof Post)
        {
            return;
        }
        $filename = $e->getImage();
        if($filename)
        {
            $e->setImage(null);
            @unlink($filename);

            // clear folder
            $web_folder = $this->kernel_rootdir.'/../web/';
            $folder = $web_folder.$this->config['uploads']['folder'].'/'.Post::IMAGES_FOLDER.'/'.$e->getFolder();
            $filesystem = new Filesystem();
            $filesystem->remove(array($folder));
        }

        $this->clearCacheForAssociations($e, $e->getTopic());
    }

    private function clearCacheForAssociations($e, $topic)
    {
        $cache = $this->em->getConfiguration()->getResultCacheImpl();
        if($topic instanceof Topic)
        {
            $category = $topic->getCategory();
            if($category instanceof Category)
            {
                $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$category->getId());
                $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$category->getId().'_hydration');
            }
            $cache->delete(Topic::RESULT_CACHE_ITEM_PREFIX.$topic->getId());
            $cache->delete(Topic::RESULT_CACHE_ITEM_PREFIX.$topic->getId().'_hydration');
        }

        $thread = $e->getThread();
        if($thread instanceof Post)
        {
            $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$thread->getId());
            $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$thread->getId().'_hydration');
        }
    }
}