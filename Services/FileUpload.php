<?php

namespace XLabs\ForumBundle\Services;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

class FileUpload
{
    private $kernel_rootdir;

    public function __construct($kernel_rootdir)
    {
        $this->kernel_rootdir = $kernel_rootdir;
    }

    public function upload($file, $targetDir, $destination_filename = false)
    {
        if(is_null($file))
        {
            return $file;
        }
        if(!($file instanceof UploadedFile))
        {
            return false;
        }

        $targetDir = rtrim($targetDir, '/').'/';
        $path = $this->kernel_rootdir.'/../web/'.$targetDir;

        $filesystem = new Filesystem();
        try {
            $filesystem->mkdir($path, 0777);
        } catch (IOExceptionInterface $exception) {
            echo 'An error occurred while creating your directory at '.$exception->getPath();
        }
        $filename = $this->uploadFile($file, $path, $destination_filename);
        @chmod($path.$filename, 0777);
        return $targetDir.$filename;
    }

    public function uploadFile($file, $destination_folder, $destination_filename = false)
    {
        $fileName = ($destination_filename ? $destination_filename : md5(uniqid())).'.'.$file->guessExtension();
        $file->move($destination_folder, $fileName);
        return $fileName;
    }
}