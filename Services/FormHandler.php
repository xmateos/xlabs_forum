<?php

namespace XLabs\ForumBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormError;
use \Exception;

class FormHandler
{
    private $request;
    private $em;

    public function __construct(RequestStack $requestStack, EntityManagerInterface $em)
    {
        $this->request = $requestStack->getCurrentRequest();
        $this->em = $em;
    }

    public function handle(&$form, $entity)
    {
        $form->handleRequest($this->request);
        if($form->isSubmitted() && $form->isValid())
        {
            try {
                $this->em->persist($entity);
                $this->em->flush();
                return true;
            } catch(Exception $e) {
                return false;
                //$form->addError(new FormError($e->getMessage()));
            }
        }
        return false;
    }
}