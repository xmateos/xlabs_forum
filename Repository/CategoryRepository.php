<?php

namespace XLabs\ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use XLabs\ForumBundle\Entity\Category;
use XLabs\ForumBundle\Entity\Post;
use Doctrine\DBAL\Cache\QueryCacheProfile;

class CategoryRepository extends EntityRepository
{
    public function getCategoriesById($aParams)
    {
        $default_params = array(
            'result_ids' => false,
            'sorting' => 'name'
        );
        $aParams = array_merge($default_params, $aParams);

        if(!$aParams['result_ids'] || empty($aParams['result_ids']))
        {
            return array();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();
        $aIds_qb = $qb
            ->select('DISTINCT e.id')
            ->from(Category::class,'e');
        if(is_array($aParams['result_ids']))
        {
            $aIds_qb->where($aIds_qb->expr()->in('e.id', $aParams['result_ids']));
        } else {
            $aIds_qb->where($aIds_qb->expr()->eq('e.id', $aParams['result_ids']));
        }
        $aIds = array();
        switch($aParams['sorting'])
        {
            case 'name':
                $aIds_qb->orderBy('e.name','ASC');
                $aIds = $aIds_qb->getQuery()->getArrayResult();
                $aIds = array_map(function($v){
                    return $v['id'];
                }, $aIds);
                break;
            case 'field':
                $aIds = $aParams['result_ids'];
                // will stick to the array order
                $qb->addSelect("FIELD(e.id, ".implode(', ', $aParams['result_ids']).") AS HIDDEN sorting")
                    ->orderBy("sorting");
                break;
        }

        $results = array();
        foreach($aIds as $result_id)
        {
            $results[] = $this->getCategoryById($result_id);
        }
        return array_filter($results);
    }

    public function getCategoryById($category_id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $cache = $em->getConfiguration()->getResultCacheImpl();
        $hydrationCacheProfile = new QueryCacheProfile(Category::RESULT_CACHE_ITEM_TTL, Category::RESULT_CACHE_ITEM_PREFIX.$category_id.'_hydration', $cache);

        // Total threads
        $qbTotalThreads = $em->createQueryBuilder();
        $qbTotalThreads
            ->select('COUNT(DISTINCT p.id)')
            ->from(Post::class, 'p')
            ->join('p.topic', 'p_t')
            ->join('p_t.category', 'p_t_c')
            ->leftjoin('p.thread', 'p_th')
            ->where(
                $qbTotalThreads->expr()->andX(
                    $qbTotalThreads->expr()->eq('p_t_c.id', 'c.id'),
                    $qbTotalThreads->expr()->isNull('p_th.id')
                )
            )
        ;

        // Total replies
        $qbTotalReplies = $em->createQueryBuilder();
        $qbTotalReplies
            ->select('COUNT(DISTINCT _p.id)')
            ->from(Post::class, '_p')
            ->join('_p.topic', '_p_t')
            ->join('_p_t.category', '_p_t_c')
            ->leftjoin('_p.thread', '_p_th')
            ->where(
                $qbTotalThreads->expr()->andX(
                    $qbTotalThreads->expr()->eq('_p_t_c.id', 'c.id'),
                    $qbTotalThreads->expr()->isNotNull('_p_th.id')
                )
            )
        ;

        $category = $qb
            ->select('partial c.{id, name, canonical, creationdate, hidden, folder, cover}')
            ->addSelect('COUNT(DISTINCT c_t.id) as total_topics')
            ->addSelect('('.$qbTotalThreads->getQuery()->getDQL().') AS total_threads')
            ->addSelect('('.$qbTotalReplies->getQuery()->getDQL().') AS total_replies')
            ->from(Category::class,'c')
            ->leftjoin('c.topics', 'c_t')
            ->where($qb->expr()->eq('c.id', $category_id))
            ->getQuery()
            ->useQueryCache(true)
            ->setResultCacheLifetime(Category::RESULT_CACHE_ITEM_TTL)
            ->setResultCacheId(Category::RESULT_CACHE_ITEM_PREFIX.$category_id)
            ->setHydrationCacheProfile($hydrationCacheProfile)
            ->getArrayResult();
        if($category)
        {
            $category = $category[0];
            $category[0]['total_topics'] = $category['total_topics'];
            unset($category['total_topics']);
            $category[0]['total_threads'] = $category['total_threads'];
            unset($category['total_threads']);
            $category[0]['total_replies'] = $category['total_replies'];
            unset($category['total_replies']);
            return $category[0];
        } else {
            $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$category_id);
            $cache->delete(Category::RESULT_CACHE_ITEM_PREFIX.$category_id.'_hydration');
            return false;
        }
    }
}