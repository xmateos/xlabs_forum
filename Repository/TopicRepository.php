<?php

namespace XLabs\ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use XLabs\ForumBundle\Entity\Topic;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use XLabs\ForumBundle\Entity\Post;
use XLabs\ForumBundle\Helpers\DT;

class TopicRepository extends EntityRepository
{
    public function getTopicsById($aParams)
    {
        $default_params = array(
            'result_ids' => false,
            'sorting' => 'name'
        );
        $aParams = array_merge($default_params, $aParams);

        if(!$aParams['result_ids'] || empty($aParams['result_ids']))
        {
            return array();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();
        $aIds_qb = $qb
            ->select('DISTINCT e.id')
            ->from(Topic::class,'e');
        if(is_array($aParams['result_ids']))
        {
            $aIds_qb->where($aIds_qb->expr()->in('e.id', $aParams['result_ids']));
        } else {
            $aIds_qb->where($aIds_qb->expr()->eq('e.id', $aParams['result_ids']));
        }
        $aIds = array();
        switch($aParams['sorting'])
        {
            case 'name':
                $aIds_qb->orderBy('e.name','ASC');
                $aIds = $aIds_qb->getQuery()->getArrayResult();
                $aIds = array_map(function($v){
                    return $v['id'];
                }, $aIds);
                break;
            case 'field':
                $aIds = $aParams['result_ids'];
                // will stick to the array order
                $qb->addSelect("FIELD(e.id, ".implode(', ', $aParams['result_ids']).") AS HIDDEN sorting")
                    ->orderBy("sorting");
                break;
        }

        $results = array();
        foreach($aIds as $result_id)
        {
            $results[] = $this->getTopicById($result_id);
        }
        return array_filter($results);
    }

    public function getTopicById($topic_id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $cache = $em->getConfiguration()->getResultCacheImpl();
        $hydrationCacheProfile = new QueryCacheProfile(Topic::RESULT_CACHE_ITEM_TTL, Topic::RESULT_CACHE_ITEM_PREFIX.$topic_id.'_hydration', $cache);

        // Last topic´s post
        $qbLastPost = $em->createQueryBuilder();
        $qbLastPost
            ->select($qbLastPost->expr()->max('p.id'))
            ->from(Post::class, 'p')
            ->join('p.topic', 'p_t')
            ->where(
                $qbLastPost->expr()->andX(
                    $qbLastPost->expr()->eq('p_t.id', 't.id')
                )
            )
            ->orderBy('p.id', 'DESC')
            ->groupBy('p_t.id')
        ;

        // Total threads
        $qbTotalThreads = $em->createQueryBuilder();
        $qbTotalThreads
            ->select('COUNT(DISTINCT _p.id)')
            ->from(Post::class, '_p')
            ->join('_p.topic', '_p_t')
            ->leftjoin('_p.thread', '_p_th')
            ->where(
                $qbTotalThreads->expr()->andX(
                    $qbTotalThreads->expr()->eq('_p_t.id', 't.id'),
                    $qbTotalThreads->expr()->isNull('_p_th.id')
                )
            )
        ;

        // Total replies
        $qbTotalReplies = $em->createQueryBuilder();
        $qbTotalReplies
            ->select('COUNT(DISTINCT __p.id)')
            ->from(Post::class, '__p')
            ->join('__p.topic', '__p_t')
            ->leftjoin('__p.thread', '__p_th')
            ->where(
                $qbTotalReplies->expr()->andX(
                    $qbTotalReplies->expr()->eq('__p_t.id', 't.id'),
                    $qbTotalReplies->expr()->isNotNull('__p_th.id')
                )
            )
        ;

        $topic = $qb
            ->select('partial t.{id, title, canonical, body, creationdate, hidden, folder, cover, status, pinned}')
            ->addSelect('t_a')
            ->addSelect('partial t_c.{id, name, canonical, creationdate, hidden, folder, cover}')
            ->addSelect('('.$qbTotalThreads->getQuery()->getDQL().') AS total_threads')
            ->addSelect('('.$qbTotalReplies->getQuery()->getDQL().') AS total_replies')
            ->addSelect('__t_p')
            ->addSelect('__t_p_a')
            ->from(Topic::class,'t')
            ->join('t.author', 't_a')
            ->leftjoin('t.category', 't_c')
            ->leftjoin('t.posts', '__t_p', 'WITH', $qb->expr()->in('__t_p.id', $qbLastPost->getQuery()->getDQL())) // last post
            ->leftjoin('__t_p.author', '__t_p_a')
            ->where($qb->expr()->eq('t.id', $topic_id))
            ->getQuery()
            ->useQueryCache(true)
            ->setResultCacheLifetime(Topic::RESULT_CACHE_ITEM_TTL)
            ->setResultCacheId(Topic::RESULT_CACHE_ITEM_PREFIX.$topic_id)
            ->setHydrationCacheProfile($hydrationCacheProfile)
            ->getArrayResult();

        if($topic)
        {
            $topic = $topic[0];
            $topic[0]['total_threads'] = $topic['total_threads'];
            unset($topic['total_threads']);
            $topic[0]['total_replies'] = $topic['total_replies'];
            unset($topic['total_replies']);
            $topic[0]['creationdate_human'] = DT::humanDT($topic[0]['creationdate']);
            if($topic[0]['posts'])
            {
                $last_post = $topic[0]['posts'][0];
                $last_post['publishdate_human'] = DT::humanDT($last_post['publishdate']);
                $topic[0]['last_post'] = $last_post;
                unset($topic['posts']);
            }
            return $topic[0];
        } else {
            $cache->delete(Topic::RESULT_CACHE_ITEM_PREFIX.$topic_id);
            $cache->delete(Topic::RESULT_CACHE_ITEM_PREFIX.$topic_id.'_hydration');
            return false;
        }
    }
}