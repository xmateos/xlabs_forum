<?php

namespace XLabs\ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use XLabs\ForumBundle\Entity\Post;
use Doctrine\DBAL\Cache\QueryCacheProfile;
use XLabs\ForumBundle\Helpers\DT;

class PostRepository extends EntityRepository
{
    public function getPostsById($aParams)
    {
        $default_params = array(
            'result_ids' => false,
            'sorting' => 'name'
        );
        $aParams = array_merge($default_params, $aParams);

        if(!$aParams['result_ids'] || empty($aParams['result_ids']))
        {
            return array();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();
        $aIds_qb = $qb
            ->select('DISTINCT e.id')
            ->from(Post::class,'e');
        if(is_array($aParams['result_ids']))
        {
            $aIds_qb->where($aIds_qb->expr()->in('e.id', $aParams['result_ids']));
        } else {
            $aIds_qb->where($aIds_qb->expr()->eq('e.id', $aParams['result_ids']));
        }
        $aIds = array();
        switch($aParams['sorting'])
        {
            case 'name':
                $aIds_qb->orderBy('e.name','ASC');
                $aIds = $aIds_qb->getQuery()->getArrayResult();
                $aIds = array_map(function($v){
                    return $v['id'];
                }, $aIds);
                break;
            case 'field':
                $aIds = $aParams['result_ids'];
                // will stick to the array order
                $qb->addSelect("FIELD(e.id, ".implode(', ', $aParams['result_ids']).") AS HIDDEN sorting")
                    ->orderBy("sorting");
                break;
        }

        $results = array();
        foreach($aIds as $result_id)
        {
            $results[] = $this->getPostById($result_id);
        }
        return array_filter($results);
    }

    public function getPostById($post_id)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder();
        $cache = $em->getConfiguration()->getResultCacheImpl();
        $hydrationCacheProfile = new QueryCacheProfile(Post::RESULT_CACHE_ITEM_TTL, Post::RESULT_CACHE_ITEM_PREFIX.$post_id.'_hydration', $cache);

        // Total replies
        $qbTotalReplies = $em->createQueryBuilder();
        $qbTotalReplies
            ->select('COUNT(DISTINCT _p.id)')
            ->from(Post::class, '_p')
            ->join('_p.thread', '_p_t')
            ->where(
                $qbTotalReplies->expr()->eq('_p_t.id', 'p.id')
            )
        ;

        // Last reply
        $qbLastReply = $em->createQueryBuilder();
        $qbLastReply
            ->select($qbLastReply->expr()->max('__p.creationdate'))
            ->from(Post::class, '__p')
            ->join('__p.thread', '__p_t')
            ->where(
                $qbLastReply->expr()->eq('__p_t.id', 'p.id')
            )
            ->orderBy('__p.id', 'DESC')
        ;

        // Author total posts
        $qbAuthorTotalPosts = $em->createQueryBuilder();
        $qbAuthorTotalPosts
            ->select('COUNT(DISTINCT ___p.id)')
            ->from(Post::class, '___p')
            ->join('___p.author', '___p_a')
            ->where(
                $qbAuthorTotalPosts->expr()->eq('___p_a.id', 'p_a.id')
            )
        ;

        $post = $qb
            ->select('partial p.{id, title, body, creationdate, publishdate, hidden, folder, image, status}')
            ->addSelect('p_a')
            ->addSelect('partial p_t.{id, title, canonical, creationdate, hidden, folder, cover}')
            ->addSelect('p_t_a')
            ->addSelect('partial p_t_c.{id, name, canonical, creationdate, hidden, folder, cover}')
            ->addSelect('partial p_th.{id, title, body, creationdate, publishdate, hidden, folder, image, status}')
            ->addSelect('p_th_a')
            ->addSelect('partial p_q.{id, title, body, creationdate, publishdate, hidden, folder, image, status}')
            ->addSelect('p_q_a')
            ->addSelect('('.$qbTotalReplies->getQuery()->getDQL().') AS total_replies')
            ->addSelect('('.$qbLastReply->getQuery()->getDQL().') AS last_reply_creationdate')
            ->addSelect('('.$qbAuthorTotalPosts->getQuery()->getDQL().') AS author_total_posts')
            ->from(Post::class,'p')
            ->join('p.author', 'p_a')
            ->leftjoin('p.topic', 'p_t')
            ->join('p_t.author', 'p_t_a')
            ->leftjoin('p_t.category', 'p_t_c')
            ->leftjoin('p.thread', 'p_th')
            ->leftjoin('p_th.author', 'p_th_a')
            ->leftjoin('p.quoted', 'p_q')
            ->leftjoin('p_q.author', 'p_q_a')
            ->where($qb->expr()->eq('p.id', $post_id))
            ->getQuery()
            ->useQueryCache(true)
            ->setResultCacheLifetime(Post::RESULT_CACHE_ITEM_TTL)
            ->setResultCacheId(Post::RESULT_CACHE_ITEM_PREFIX.$post_id)
            ->setHydrationCacheProfile($hydrationCacheProfile)
            ->getArrayResult();
        if($post)
        {
            $post = $post[0];
            $post[0]['creationdate_human'] = DT::humanDT($post[0]['creationdate']);
            $post[0]['publishdate_human'] = DT::humanDT($post[0]['publishdate']);
            $post[0]['total_replies'] = $post['total_replies'];
            unset($post['total_replies']);
            $post[0]['last_reply_creationdate'] = $post['last_reply_creationdate'];
            unset($post['last_reply_creationdate']);
            $post[0]['author']['total_posts'] = $post['author_total_posts'];
            unset($post['author_total_posts']);
            return $post[0];
        } else {
            $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post_id);
            $cache->delete(Post::RESULT_CACHE_ITEM_PREFIX.$post_id.'_hydration');
            return false;
        }
    }
}