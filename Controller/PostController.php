<?php

namespace XLabs\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\ForumBundle\Entity\Post;
use XLabs\ForumBundle\Entity\Topic;
use XLabs\ForumBundle\Form\PostType;
use XLabs\ForumBundle\Services\FormHandler;
use XLabs\MMAdminBundle\Annotations as XLabsMMAdmin;

/**
 * @Route(name="xlabs_forum_post_")
 * @XLabsMMAdmin\isProtected
 */
class PostController extends Controller
{
    /**
     * @Route("/form/for_topic/{topic_id}", name="form_for_topic", defaults={"post_id": false, "thread_id": false, "quoted_id": false}, options={"expose"=true})
     * @Route("/form/reply/for_thread/{thread_id}", name="form_for_thread", defaults={"topic_id": false, "quoted_id": false, "post_id": false}, options={"expose"=true})
     * @Route("/form/quote/{quoted_id}", name="form_quote_post", defaults={"topic_id": false, "thread_id": false, "post_id": false}, options={"expose"=true})
     * @Route("/form/{post_id}", name="form", defaults={"post_id": false, "topic_id": false, "thread_id": false, "quoted_id": false}, options={"expose"=true})
     */
    public function formAction(Request $request, EntityManagerInterface $em, FormHandler $formHandler, $post_id, $topic_id, $thread_id, $quoted_id)
    {
        $post = $post_id ? $em->getRepository(Post::class)->find($post_id) : new Post();

        if($topic_id)
        {
            $topic = $em->getRepository(Topic::class)->find($topic_id);
            $post->setTopic($topic);
        }
        if($thread_id)
        {
            $thread = $em->getRepository(Post::class)->find($thread_id);
            $post->setThread($thread);
            $post->setTopic($thread->getTopic());
        }
        if($quoted_id)
        {
            $quoted = $em->getRepository(Post::class)->find($quoted_id);
            $post->setQuoted($quoted);
            $post->setThread($quoted->getThread() ? $quoted->getThread() : $quoted);
            $post->setTopic($quoted->getThread() ? $quoted->getThread()->getTopic() : $quoted->getTopic());
        }

        if(!$post_id)
        {
            $config = $this->getParameter('xlabs_forum_config');
            $author = $em->getRepository($config['user_entity'])->findOneBy(array(
                'username' => $config['admin_user']
            ));
            $post->setAuthor($author);
        }

        $form = $this->createForm(PostType::class, $post, array(
            'xlabs_forum_config' => $this->getParameter('xlabs_forum_config')
        ));
        $form_success = $formHandler->handle($form, $post);
        if($form_success)
        {
            return $this->render('@XLabsForum/fancybox_close.html.twig');
        }

        return $this->render('@XLabsForum/Admin/Post/form.html.twig', array(
            'form' => $form->createView(),
            'post' => $post
        ));
    }

    /**
     * @Route("/reload/{post_id}", name="reload", options={"expose"=true})
     * to reload table items after editing
     */
    public function reloadAction(Request $request, EntityManagerInterface $em, $post_id)
    {
        $post = $em->getRepository(Post::class)->getPostById($post_id);
        return $this->render('@XLabsForum/Admin/Post/item.html.twig', array(
            'item' => $post
        ));
    }

    /**
     * @Route("/delete/{post_id}", name="delete", options={"expose"=true})
     */
    public function deleteAction(EntityManagerInterface $em, $post_id)
    {
        $post = $em->getRepository(Post::class)->find($post_id);
        $em->remove($post);
        $em->flush();
        return new Response('ok');
    }

    /**
     * @Route("/approve/{post_id}", name="approve", options={"expose"=true})
     */
    public function approveAction(EntityManagerInterface $em, $post_id)
    {
        $post = $em->getRepository(Post::class)->find($post_id);
        $post->setStatus(Post::STATUS_APPROVED);
        $em->persist($post);
        $em->flush();
        return new Response('ok');
    }
}
