<?php

namespace XLabs\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\ForumBundle\Entity\Category;
use XLabs\ForumBundle\Entity\Topic;
use XLabs\ForumBundle\Form\TopicType;
use XLabs\ForumBundle\Services\FormHandler;
use XLabs\MMAdminBundle\Annotations as XLabsMMAdmin;

/**
 * @Route(name="xlabs_forum_topic_")
 * @XLabsMMAdmin\isProtected
 */
class TopicController extends Controller
{
    /**
     * @Route("/form/for_category/{category_id}", name="form_for_category", defaults={"topic_id": false}, options={"expose"=true})
     * @Route("/form/{topic_id}", name="form", defaults={"topic_id": false, "category_id": false}, options={"expose"=true})
     */
    public function formAction(Request $request, EntityManagerInterface $em, FormHandler $formHandler, $topic_id, $category_id)
    {
        $topic = $topic_id ? $em->getRepository(Topic::class)->find($topic_id) : new Topic();

        $config = $this->getParameter('xlabs_forum_config');
        $author = $em->getRepository($config['user_entity'])->findOneBy(array(
            'username' => $config['admin_user']
        ));
        $topic->setAuthor($author);

        if($category_id)
        {
            $category = $em->getRepository(Category::class)->find($category_id);
            $topic->setCategory($category);
        }

        $form = $this->createForm(TopicType::class, $topic, array(
            'xlabs_forum_config' => $this->getParameter('xlabs_forum_config')
        ));
        $form_success = $formHandler->handle($form, $topic);
        if($form_success)
        {
            return $this->render('@XLabsForum/fancybox_close.html.twig');
        }

        return $this->render('@XLabsForum/Admin/Topic/form.html.twig', array(
            'form' => $form->createView(),
            'topic' => $topic
        ));
    }

    /**
     * @Route("/reload/{topic_id}", name="reload", options={"expose"=true})
     * to reload table items after editing
     */
    public function reloadAction(Request $request, EntityManagerInterface $em, $topic_id)
    {
        $topic = $em->getRepository(Topic::class)->getTopicById($topic_id);
        return $this->render('@XLabsForum/Admin/Topic/item.html.twig', array(
            'item' => $topic
        ));
    }

    /**
     * @Route("/delete/{topic_id}", name="delete", options={"expose"=true})
     */
    public function deleteAction(EntityManagerInterface $em, $topic_id)
    {
        $topic = $em->getRepository(Topic::class)->find($topic_id);
        $em->remove($topic);
        $em->flush();
        return new Response('ok');
    }

    /**
     * @Route("/approve/{topic_id}", name="approve", options={"expose"=true})
     */
    public function approveAction(EntityManagerInterface $em, $topic_id)
    {
        $topic = $em->getRepository(Topic::class)->find($topic_id);
        $topic->setStatus(Topic::STATUS_APPROVED);
        $em->persist($topic);
        $em->flush();
        return new Response('ok');
    }
}
