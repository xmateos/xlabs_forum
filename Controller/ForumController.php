<?php

namespace XLabs\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use XLabs\ForumBundle\Entity\Topic;
use XLabs\ForumBundle\Form\TopicType;
use XLabs\ForumBundle\Entity\Post;
use XLabs\ForumBundle\Form\PostType;
use \Exception;
use Symfony\Component\Form\FormError;
use XLabs\ForumBundle\Services\FormHandler;
use XLabs\ForumBundle\Event\Topic\OnRequested;
use XLabs\ForumBundle\Event\Post\OnCreate;
use XLabs\ForumBundle\Search\Post as SearchPost;

class ForumController extends Controller
{
    public function indexAction()
    {
        return $this->render('@XLabsForum/Forum/index.html.twig');
    }

    /**
     * @Route("/request/topic", name="xlabs_forum_request_topic", defaults={"topic_id": false}, options={"expose"=true})
     */
    public function requestTopicAction(Request $request, RouterInterface $router, FormHandler $formHandler, EventDispatcherInterface $event_dispatcher)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $topic = new Topic();
        $topic->setAuthor($user);
        $topic->setStatus(Topic::STATUS_WAITING_FOR_APPROVAL);

        $form = $this->createForm(TopicType::class, $topic, array(
            'xlabs_forum_config' => $this->getParameter('xlabs_forum_config'),
            'action' => $router->generate('xlabs_forum_request_topic')
        ));
        // frontend tweak
        $aFieldsToHide = array('hidden', 'status', 'pinned', 'preserve_html');
        foreach($aFieldsToHide as $fieldToHide)
        {
            if($form->has($fieldToHide))
            {
                $form->remove($fieldToHide);
            }
        }
        $form_success = $formHandler->handle($form, $topic);
        if($form_success)
        {
            $event = new OnRequested(array(
                'topic' => $topic
            ));
            $event_dispatcher->dispatch($event::NAME, $event);
            return $this->render('@XLabsForum/page_reload.html.twig');
        }

        return $this->render('@XLabsForum/Forum/Topic/form.html.twig', array(
            'form' => $form->createView(),
            'topic' => $topic
        ));
    }

    public function topicAction(EntityManagerInterface $em, $topic_id)
    {
        $topic = $em->getRepository(Topic::class)->getTopicById($topic_id);
        return $this->render('@XLabsForum/Forum/topic_details.html.twig', array(
            'topic' => $topic
        ));
    }

    /**
     * @Route("/new/thread/{topic_id}", name="xlabs_forum_new_thread", defaults={"topic_id": false}, options={"expose"=true})
     */
    public function newThreadAction(Request $request, EntityManagerInterface $em, RouterInterface $router, FormHandler $formHandler, EventDispatcherInterface $event_dispatcher, $topic_id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $topic = $em->getRepository(Topic::class)->find($topic_id);

        $post = new Post();
        $post->setStatus(Post::STATUS_WAITING_FOR_APPROVAL);
        $post->setAuthor($user);
        $post->setTopic($topic);

        $form = $this->createForm(PostType::class, $post, array(
            'xlabs_forum_config' => $this->getParameter('xlabs_forum_config'),
            'action' => $router->generate('xlabs_forum_new_thread', array(
                'topic_id' => $topic_id
            ))
        ));
        // frontend tweak
        $aFieldsToHide = array('author', 'category', 'hidden', 'status', 'publishdate', 'topic', 'preserve_html');
        foreach($aFieldsToHide as $fieldToHide)
        {
            if($form->has($fieldToHide))
            {
                $form->remove($fieldToHide);
            }
        }
        $form_success = $formHandler->handle($form, $post);
        $response = null;
        if($form_success)
        {
            $event = new OnCreate(array(
                'thread' => $post
            ));
            $event_dispatcher->dispatch($event::NAME, $event);
            return $this->render('@XLabsForum/page_reload.html.twig');
        } elseif($form->isSubmitted()) {
            // error
            $response = new Response();
            $response->setStatusCode(208); // specific code for the frontend
        }

        return $this->render('@XLabsForum/Forum/Post/form.html.twig', array(
            'form' => $form->createView(),
            'post' => $post
        ), $response);
    }

    public function threadAction(EntityManagerInterface $em, $thread_id)
    {
        $thread = $em->getRepository(Post::class)->getPostById($thread_id);
        return $this->render('@XLabsForum/Forum/thread_details.html.twig', array(
            'thread' => $thread
        ));
    }

    /**
     * @Route("/new/reply/{thread_id}", name="xlabs_forum_new_reply", options={"expose"=true})
     */
    public function replyAction(Request $request, EntityManagerInterface $em, RouterInterface $router, FormHandler $formHandler, EventDispatcherInterface $event_dispatcher, $thread_id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $thread = $em->getRepository(Post::class)->find($thread_id);

        $post = new Post();
        $post->setAuthor($user);
        $post->setThread($thread);
        $post->setTopic($thread->getTopic());
        $post->setStatus(Post::STATUS_APPROVED);

        $form = $this->createForm(PostType::class, $post, array(
            'xlabs_forum_config' => $this->getParameter('xlabs_forum_config'),
            'action' => $router->generate('xlabs_forum_new_reply', array(
                'thread_id' => $thread_id
            ))
        ));
        // frontend tweak
        $aFieldsToHide = array('title', 'author', 'publishdate', 'hidden', 'status', 'topic', 'preserve_html');
        foreach($aFieldsToHide as $fieldToHide)
        {
            if($form->has($fieldToHide))
            {
                $form->remove($fieldToHide);
            }
        }
        $form_success = $formHandler->handle($form, $post);
        $response = null;
        if($form_success)
        {
            /*$event = new OnRequested(array(
                'topic' => $topic
            ));
            $event_dispatcher->dispatch($event::NAME, $event);*/
            return $this->render('@XLabsForum/page_reload.html.twig');
        } elseif($form->isSubmitted()) {
            // error
            $response = new Response();
            $response->setStatusCode(208); // specific code for the frontend
        }

        return $this->render('@XLabsForum/Forum/Post/form.html.twig', array(
            'form' => $form->createView(),
            'post' => $post
        ), $response);
    }

    /**
     * @Route("/new/quote/{post_id}", name="xlabs_forum_new_quote", options={"expose"=true})
     */
    public function quoteAction(Request $request, EntityManagerInterface $em, RouterInterface $router, FormHandler $formHandler, EventDispatcherInterface $event_dispatcher, $post_id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        //$thread = $em->getRepository(Post::class)->find($thread_id);
        $quoted = $em->getRepository(Post::class)->find($post_id);

        $post = new Post();
        $post->setAuthor($user);
        $post->setThread($quoted->getThread() ? $quoted->getThread() : $quoted);
        $post->setTopic($quoted->getTopic());
        $post->setStatus(Post::STATUS_APPROVED);
        $post->setQuoted($quoted);

        $form = $this->createForm(PostType::class, $post, array(
            'xlabs_forum_config' => $this->getParameter('xlabs_forum_config'),
            'action' => $router->generate('xlabs_forum_new_quote', array(
                'post_id' => $post_id
            ))
        ));
        // frontend tweak
        $aFieldsToHide = array('title', 'author', 'publishdate', 'hidden', 'status', 'topic', 'preserve_html');
        foreach($aFieldsToHide as $fieldToHide)
        {
            if($form->has($fieldToHide))
            {
                $form->remove($fieldToHide);
            }
        }

        $form_success = $formHandler->handle($form, $post);
        $response = null;
        if($form_success)
        {
            /*$event = new OnRequested(array(
                'topic' => $topic
            ));
            $event_dispatcher->dispatch($event::NAME, $event);*/
            return $this->render('@XLabsForum/page_reload.html.twig');
        } elseif($form->isSubmitted()) {
            // error
            $response = new Response();
            $response->setStatusCode(208); // specific code for the frontend
        }

        /*if($request->getMethod() == 'POST')
        {
            $form->handleRequest($request);
            if($form->isValid())
            {
                try {
                    $quoted_html = $this->render('@XLabsForum/Forum/item/quoted.html.twig', array('post' => $quoted))->getContent();
                    $post->setBodyPreserveHTML($quoted_html.$post->getBody());
                    $em->persist($post);
                    $em->flush();

                    return $this->render('@XLabsForum/fancybox_close.html.twig');
                } catch(Exception $e) {
                    $form->addError(new FormError($e->getMessage()));
                    //$this->get('session')->getFlashBag()->add('mm_forum_error', $e->getMessage());
                }
            }
        }*/

        return $this->render('@XLabsForum/Forum/Post/form.html.twig', array(
            'form' => $form->createView(),
            'post' => $post
        ), $response);
    }
}
