<?php

namespace XLabs\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use XLabs\ForumBundle\Search\Category as SearchCategory;
use XLabs\ForumBundle\Search\Topic as SearchTopic;
use XLabs\ForumBundle\Search\Post as SearchPost;
use XLabs\MMAdminBundle\Annotations as XLabsMMAdmin;

class ForumAdminController extends Controller
{
    /**
     * @XLabsMMAdmin\isProtected
     */
    public function indexAction()
    {
        return $this->render('@XLabsForum/Admin/index.html.twig');
        // get categories

        // get latest topics
    }

    private function fixBooleanValues(&$aParams)
    {
        array_walk_recursive($aParams, function(&$aParam){
            if(is_string($aParam))
            {
                switch($aParam)
                {
                    case 'true':
                        $aParam = true;
                        break;
                    case 'false':
                        $aParam = false;
                        break;
                }
            }
        });
    }

    public function renderResultsAction(Request $request, RequestStack $request_stack)
    {
        $master_request = $request_stack->getMasterRequest();
        $em = $this->getDoctrine()->getManager();

        $aParams = $request->get('aParams');
        $aParams = $aParams ? $aParams : $request->request->get('aParams');

        $contentType = strtolower($aParams['contentType']);

        if($aParams)
        {
            $aParams = is_array($aParams) ? $aParams : json_decode($request->request->get('aParams'), true);
            $this->fixBooleanValues($aParams);

            // IDs used on the same master request
            $excluded_ids = array($contentType => array());
            if(!array_key_exists('allow_duplicates_in_same_master_request', $aParams) || !$aParams['allow_duplicates_in_same_master_request'])
            {
                $excluded_ids = $master_request->request->get('excluded_ids');
                $excluded_ids = $excluded_ids ? $excluded_ids : array($contentType => array());
                $aParams['aExcluded_ids'] = array_key_exists('aExcluded_ids', $aParams) && is_array($aParams['aExcluded_ids']) ? array_merge($aParams['aExcluded_ids'], $excluded_ids[$contentType]) : $excluded_ids[$contentType];
                $aParams['aExcluded_ids'] = array_unique($aParams['aExcluded_ids']);
            }

            switch($contentType)
            {
                case 'categories':
                    $search = $this->get(SearchCategory::class);
                    break;
                case 'topics':
                    $search = $this->get(SearchTopic::class);
                    break;
                case 'posts':
                    $search = $this->get(SearchPost::class);
                    break;
            }
            $search_results = $search->get($aParams);

            // Store new values to exclude them in the same master request
            foreach($search_results['results'] as $result)
            {
                $excluded_ids[$contentType][] = $result['id'];
            }
            $master_request->request->set('excluded_ids', $excluded_ids);

            /*if(array_key_exists('prepend_selection', $aParams) && $aParams['prepend_selection'] && is_numeric($aParams['prepend_selection']))
            {
                $prepend_selection = $em->getRepository(Post::class)->getPostById($aParams['prepend_selection']);
                if($prepend_selection)
                {
                    array_unshift($search_results['results'], $prepend_selection);
                }

                // Remove duplicate (in case the prepend selection elements where already in the search results
                $aTmp = array_unique(array_column($search_results['results'], 'id'));
                $search_results['results'] = array_intersect_key($search_results['results'], $aTmp);
            }*/

            return $this->render($aParams['results_template'], array(
                'results' => $search_results['results'],
                'pagination' => $search_results['pagination'],
                'aParams' => $aParams
            ));
        }
        throw new NotFoundHttpException('Wrong parameters sent');
    }
}
