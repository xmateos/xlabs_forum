<?php

namespace XLabs\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use XLabs\ForumBundle\Entity\Category;
use XLabs\ForumBundle\Form\CategoryType;
use XLabs\ForumBundle\Services\FormHandler;
use XLabs\MMAdminBundle\Annotations as XLabsMMAdmin;

/**
 * @Route(name="xlabs_forum_category_")
 * @XLabsMMAdmin\isProtected
 */
class CategoryController extends Controller
{
    /**
     * @Route("/form/{category_id}", name="form", defaults={"category_id": false}, options={"expose"=true})
     */
    public function formAction(Request $request, EntityManagerInterface $em, FormHandler $formHandler, $category_id)
    {
        $category = $category_id ? $em->getRepository(Category::class)->find($category_id) : new Category();

        $form = $this->createForm(CategoryType::class, $category, array(
            'xlabs_forum_config' => $this->getParameter('xlabs_forum_config')
        ));
        $form_success = $formHandler->handle($form, $category);
        if($form_success)
        {
            return $this->render('@XLabsForum/fancybox_close.html.twig');
        }

        return $this->render('@XLabsForum/Admin/Category/form.html.twig', array(
            'form' => $form->createView(),
            'category' => $category
        ));
    }

    /**
     * @Route("/reload/{category_id}", name="reload", options={"expose"=true})
     * to reload table items after editing
     */
    public function reloadAction(Request $request, EntityManagerInterface $em, $category_id)
    {
        $category = $em->getRepository(Category::class)->getCategoryById($category_id);
        return $this->render('@XLabsForum/Admin/Category/item.html.twig', array(
            'item' => $category
        ));
    }

    /**
     * @Route("/delete/{category_id}", name="delete", options={"expose"=true})
     */
    public function deleteAction(EntityManagerInterface $em, $category_id)
    {
        $category = $em->getRepository(Category::class)->find($category_id);
        $em->remove($category);
        $em->flush();
        return new Response('ok');
    }
}
