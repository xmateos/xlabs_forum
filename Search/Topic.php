<?php

namespace XLabs\ForumBundle\Search;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use XLabs\ForumBundle\Entity\Topic as Entity;
use XLabs\ForumBundle\Entity\Post;
use \DateTime;

class Topic
{
    private $em;
    private $paginator;

    public function __construct(EntityManagerInterface $em, PaginatorInterface $knp_paginator)
    {
        $this->em = $em;
        $this->paginator = $knp_paginator;
    }

    public function get($options)
    {
        $default_options = array(
            'category_ids' => false,
            'author_ids' => false,
            'ids_collection' => false,
            'sorting' => 'name',
            'max_results' => false,
            'page' => 1,
            'aStatus' => array(
                Entity::STATUS_APPROVED,
                Entity::STATUS_WAITING_FOR_APPROVAL
            ),
            'creation_date_limit' => array( // creationdate
                'min' => false,
                'max' => false
            ),
            'aExcluded_ids' => false,
            'show_hidden' => false,
            'show_approved_only' => false,
            'show_pagination' => false,
            'containing_str' => false,
            'no_cache' => false,
            'paginator_params' => array() // paginator overridden params, for the pagination template
        );
        $aOptions = array_merge($default_options,$options);

        // When on ajax, params come as POST params, but booleans turned into strings
        array_walk_recursive($aOptions, function(&$aOption){
            if(is_string($aOption))
            {
                switch($aOption)
                {
                    case 'true':
                        $aOption = true;
                        break;
                    case 'false':
                        $aOption = false;
                        break;
                }
            }
        });

        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('partial e.{id}')
            ->from(Entity::class,'e');

        // Static collection of IDs
        if(is_array($aOptions['ids_collection']))
        {
            if(empty($aOptions['ids_collection']))
            {
                // 'ids_collection' rules over any other entry; if it comes empty, show empty results
                return array(
                    'results' => array(),
                    'pagination' => false
                );
            } else {
                $qb->andWhere(
                    $qb->expr()->in('e.id', $aOptions['ids_collection'])
                );
            }
        }

        // Author ids
        if($aOptions['author_ids'])
        {
            if(empty($aOptions['author_ids']))
            {
                // 'category_ids' rules over any other entry; if it comes empty, show empty results
                return array(
                    'results' => $aOptions['return_count'] ? 0 : array(),
                    'pagination' => false
                );
            } else {
                $qb
                    ->join('e.author','e_a', 'WITH', $qb->expr()->in('e_a.id', $aOptions['author_ids']))
                ;
            }
        }

        // Category ids
        if($aOptions['category_ids'])
        {
            if(empty($aOptions['category_ids']))
            {
                // 'category_ids' rules over any other entry; if it comes empty, show empty results
                return array(
                    'results' => $aOptions['return_count'] ? 0 : array(),
                    'pagination' => false
                );
            } else {
                $qb
                    ->join('e.category','e_c', 'WITH', $qb->expr()->in('e_c.id', $aOptions['category_ids']))
                ;
            }
        }

        // Status
        $qb->andWhere(
            $qb->expr()->in('e.status', $aOptions['aStatus'])
        );

        if($aOptions['creation_date_limit']['min'])
        {
            $min_date = new DateTime($aOptions['creation_date_limit']['min']);
            $qb->andWhere(
                $qb->expr()->gte('DATE(e.creationdate)', $qb->expr()->literal($min_date->format('Y-m-d')))
            );
        }
        if($aOptions['creation_date_limit']['max'])
        {
            $max_date = new DateTime($aOptions['creation_date_limit']['max']);
            $qb->andWhere(
                $qb->expr()->lte('DATE(e.creationdate)', $qb->expr()->literal($max_date->format('Y-m-d')))
            );
        }

        if(!empty($aOptions['aExcluded_ids']))
        {
            $qb->andWhere(
                $qb->expr()->notIn('e.id', $aOptions['aExcluded_ids'])
            );
        }

        switch($aOptions['sorting'])
        {
            case 'date':
                $qb->orderBy('e.creationdate','DESC');
                break;
            case 'field': // preserve order set in 'ids_collection' param
                if(is_array($aOptions['ids_collection']) && !empty($aOptions['ids_collection']))
                {
                    $qb
                        ->addSelect("FIELD(e.id, ".implode(', ', $aOptions['ids_collection']).") AS HIDDEN sorting")
                        ->orderBy('sorting', 'ASC');
                }
                break;
            case 'last_update': // will also show pinned first
                $qbLastUpdate = $this->em->createQueryBuilder();
                $qbLastUpdate
                    ->select($qbLastUpdate->expr()->max('_p.id'))
                    ->from(Post::class, '_p')
                    ->join('_p.topic', '_p_t')
                    ->where(
                        $qbLastUpdate->expr()->andX(
                            $qbLastUpdate->expr()->eq('_p_t.id', 'e.id')
                            //$qbLastUpdate->expr()->lte('DATE(_p.publishdate)', $qbLastUpdate->expr()->literal(date('Y-m-d')))
                            //$qbLastUpdate->expr()->eq('_p.status', Post::STATUS_READY),
                            //$qbLastUpdate->expr()->neq('_p.hidden', 1)
                        )
                    )
                ;
                $qb
                    ->addSelect('('.
                        $qbLastUpdate->getQuery()->getDQL()
                        .') AS HIDDEN recent_activity_content')
                    ->orderBy('e.pinned','DESC')
                    ->addOrderBy('recent_activity_content','DESC')
                    ->addOrderBy('e.creationdate','DESC');
                break;
            case 'name':
                $qb->orderBy('e.canonical','ASC');
                break;
        }

        if(!$aOptions['show_hidden'])
        {
            $qb->andWhere(
                $qb->expr()->neq('e.hidden', 1)
            );
        }

        if($aOptions['show_approved_only'])
        {
            $qb->andWhere(
                $qb->expr()->eq('e.status', Entity::STATUS_APPROVED)
            );
        }

        if($aOptions['containing_str'])
        {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('e.title', $qb->expr()->literal('%'.$aOptions['containing_str'].'%')),
                    $qb->expr()->like('e.body', $qb->expr()->literal('%'.$aOptions['containing_str'].'%'))
                )
            );
        }

        $query = $qb->getQuery();

        if(!$aOptions['show_pagination'] && $aOptions['max_results'])
        {
            $query->setMaxResults($aOptions['max_results']);
        }

        $resultCache_id = Entity::RESULT_CACHE_COLLECTION_PREFIX.md5($query->getSQL());
        $resultCache_ttl = Entity::RESULT_CACHE_COLLECTION_TTL;

        if(!$aOptions['no_cache'])
        {
            $query
                ->useQueryCache(true)
                ->setResultCacheLifetime($resultCache_ttl)
                ->setResultCacheId($resultCache_id);
        }

        return $this->paginate(array(
            'aResults' => $query->getArrayResult(),
            'max_results' => $aOptions['max_results'],
            'page' => $aOptions['page'],
            'show_pagination' => $aOptions['show_pagination'],
            'paginator_params' => $aOptions['paginator_params']
        ));
    }

    public function paginate($aOptions)
    {
        $default_options = array(
            'aResults' => array(),
            'max_results' => false,
            'page' => 1,
            'show_pagination' => true,
            'paginator_params' => array()
        );
        $aOptions = array_merge($default_options, $aOptions);

        $aResults = array_map(function($s){
            return $s['id'];
        }, $aOptions['aResults']);

        $pagination = false;
        $result_ids = $aResults;
        if($aOptions['show_pagination'] && $aOptions['max_results'])
        {
            if($aOptions['page'] == 'last')
            {
                $pagination = $this->paginator->paginate(
                    $result_ids,
                    1,
                    $aOptions['max_results']
                );
                $aOptions['page'] = ceil($pagination->getTotalItemCount() / $aOptions['max_results']);
            }
            $pagination = $this->paginator->paginate(
                $result_ids,
                $aOptions['page'],
                $aOptions['max_results'],
                $aOptions['paginator_params']
            );
            $result_ids = $pagination->getItems();
        }

        $results = $this->em->getRepository(Entity::class)->getTopicsById(array(
            'result_ids' => $result_ids,
            'sorting' => 'field'
        ));

        return array(
            'results' => $results,
            'pagination' => $pagination
        );
    }
}